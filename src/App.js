// Import thư viện bootstrap
import 'bootstrap/dist/css/bootstrap.min.css';
// Import thư viện fontawesome
import '@fortawesome/fontawesome-free/css/all.min.css';

import './App.css';
import Header from './component/header/Header';
import Footer from './component/footer/Footer';
import Content from './component/content/Content';

function App() {
  return (
    <div>
      <Header/>
      <Content/>
      <Footer/>
    </div>
  );
}

export default App;
