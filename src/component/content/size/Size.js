import { Component } from "react";

class Size extends Component {
    render() {
        return (
            <>
                <div className="container mt-5 div-select-combo">
                    <div className="row form-group">
                        <h1>Chọn size pizza</h1>
                        <hr className='line'></hr>
                    </div>
                    <div className="row form-group mt-4">
                        <div className="col-sm-4 text-center">
                            <div className="card">
                                <div className="card-header">
                                    <h3>S (small)</h3>
                                </div>
                                <div className="card-body">
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Đường kính: <b>20cm</b></li>
                                        <li className="list-group-item">Sườn nướng: <b>2</b></li>
                                        <li className="list-group-item">Salad: <b>200g</b></li>
                                        <li className="list-group-item">Nước ngọt: <b>2</b></li>
                                        <li className="list-group-item"><h3>150.000</h3>VNĐ</li>
                                    </ul>
                                </div>
                                <div className="card-footer text-muted">
                                    <button className="btn form-control" id="btn-combo-small" data-is-selected-menu="N">Chọn</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-4 text-center">
                            <div className="card">
                                <div className="card-header bg-warning">
                                    <h3>M (medium)</h3>
                                </div>
                                <div className="card-body">
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Đường kính: <b>25cm</b></li>
                                        <li className="list-group-item">Sườn nướng: <b>4</b></li>
                                        <li className="list-group-item">Salad: <b>300g</b></li>
                                        <li className="list-group-item">Nước ngọt: <b>3</b></li>
                                        <li className="list-group-item"><h3>200.000</h3>VNĐ</li>
                                    </ul>
                                </div>
                                <div className="card-footer text-muted">
                                    <button className="btn form-control" id="btn-combo-medium" data-is-selected-menu="N">Chọn</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-4 text-center">
                            <div className="card">
                                <div className="card-header">
                                    <h3>L (large)</h3>
                                </div>
                                <div className="card-body">
                                    <ul className="list-group list-group-flush">
                                        <li className="list-group-item">Đường kính: <b>30cm</b></li>
                                        <li className="list-group-item">Sườn nướng: <b>8</b></li>
                                        <li className="list-group-item">Salad: <b>500g</b></li>
                                        <li className="list-group-item">Nước ngọt: <b>4</b></li>
                                        <li className="list-group-item"><h3>250.000</h3>VNĐ</li>
                                    </ul>
                                </div>
                                <div className="card-footer text-muted">
                                    <button className="btn form-control" id="btn-combo-large" data-is-selected-menu="N">Chọn</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
export default Size;