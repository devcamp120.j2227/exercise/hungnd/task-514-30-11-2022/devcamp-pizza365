import { Component } from "react";

class Introduce extends Component {
    render() {
        return (
            <>
                <div className="container mt-5 div-introduce">
                    <div className="row form-group">
                        <h1>Tại sao lại Pizza 365</h1>
                        <hr className="line" />
                    </div>
                    <div className="row form-group mt-4">
                        <div className="col-sm-3 border border-warning" style={{ backgroundColor: "lightgoldenrodyellow" }}>
                            <div className="row form-group">
                                <div className="col-sm-12 pt-5 text-center">
                                    <h3>Đa dạng</h3>
                                </div>
                            </div>
                            <div className="row form-group">
                                <div className="col-sm-12 pb-3">
                                    <p>Số lượng pizza đa dạng, có đầy đủ các loại pizza đang hot nhất hiện nay.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-3 border border-warning" style={{ backgroundColor: "yellow " }}>
                            <div className="row form-group">
                                <div className="col-sm-12 pt-5 text-center">
                                    <h3>Chất lượng</h3>
                                </div>
                            </div>
                            <div className="row form-group">
                                <div className="col-sm-12 pb-3">
                                    <p>Nguyên liệu sạch 100% rõ nguồn gốc quy trình chế biến đảm bảo vệ sinh an toàn thực phẩm.</p>
                                </div>

                            </div>
                        </div>
                        <div className="col-sm-3 border border-warning" style={{ backgroundColor: "lightsalmon " }}>
                            <div className="row form-group">
                                <div className="col-sm-12 pt-5 text-center">
                                    <h3>Hương vị</h3>
                                </div>
                            </div>
                            <div className="row form-group">
                                <div className="col-sm-12 pb-3">
                                    <p>Đảm bảo hương vị ngon, độc, lạ mà bạn chỉ có thể trãi nghiệm từ Pizza 365.</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-3 border border-warning" style={{ backgroundColor: "orange" }}>
                            <div className="row form-group">
                                <div className="col-sm-12 pt-5 text-center">
                                    <h3>Dịch vụ</h3>
                                </div>
                            </div>
                            <div className="row form-group">
                                <div className="col-sm-12 pb-3">
                                    <p>Nhân viên thân thiện, nhà hàng hiện đại. Dịch vụ giao hàng nhanh chất lượng, tân tiến.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
export default Introduce;