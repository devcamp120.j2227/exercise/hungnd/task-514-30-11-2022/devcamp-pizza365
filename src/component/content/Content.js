import { Component } from "react";
import Drink from "./drink/Drink";
import Form from "./form/Form";
import Introduce from "./introduce/Introduce";
import Size from "./size/Size";
import Type from "./type/Type";

class Content extends Component {
    render() {
        return(
            <>
            <Introduce/>
            <Size/>
            <Type/>
            <Drink/>
            <Form/>
            </>
        )
    }
}
export default Content;