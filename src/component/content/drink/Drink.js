import { Component } from "react";

class Drink extends Component {
    render() {
        return (
            <>
                {/* <!-- div chọn đồ uống --> */}
                <div className="container mt-5 div-select-drink">
                    <div className="row form-group">
                        <h1>Chọn đồ uống</h1>
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-12">
                            <select name="select" id="select-nuoc-uong" className="form-control">
                                <option value="all">Tất cả các loại nước uống</option>
                            </select>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
export default Drink;