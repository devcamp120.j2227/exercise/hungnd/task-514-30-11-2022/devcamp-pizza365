import { Component } from "react";

class Form extends Component {
    render() {
        return (
            <>
                {/* <!-- Div gửi đơn hàng --> */}
                <div className="container mt-5 div-send-order">
                    <div className="row form-group">
                        <h1>Gửi đơn hàng</h1>
                    </div>
                    <div className="row form-group">
                        <label for="inp-ho-va-ten">Tên</label>
                        <input type="text" id="inp-ho-va-ten" placeholder="Nhập tên" className="form-control" />
                    </div>
                    <div className="row form-group">
                        <label for="inp-email">Email</label>
                        <input type="text" id="inp-email" placeholder="Nhập email" className="form-control" />
                    </div>
                    <div className="row form-group">
                        <label for="inp-so-dien-thoai">Số điện thoại</label>
                        <input type="text" id="inp-so-dien-thoai" placeholder="Nhập số điện thoại" className="form-control" />
                    </div>
                    <div className="row form-group">
                        <label for="inp-dia-chi">Địa chỉ</label>
                        <input type="text" id="inp-dia-chi" placeholder="Nhập địa chỉ" className="form-control" />
                    </div>
                    <div className="row form-group">
                        <label for="inp-ma-giam-gia">Mã giảm giá</label>
                        <input type="text" id="inp-ma-giam-gia" placeholder="Nhập mã giảm giá" className="form-control" />
                    </div>
                    <div className="row form-group">
                        <label for="inp-loi-nhan">Lời nhắn</label>
                        <input type="text" id="inp-loi-nhan" placeholder="Nhập lời nhắn" className="form-control" />
                    </div>
                    <div className="row form-group">
                        <div className="col-sm-12">
                            <button className="btn form-control mt-3" style={{ backgroundColor: "orange" }} id="btn-send-order">Gửi</button>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
export default Form;