import { Component } from "react";

import seafood from '../../../assets/images/seafood.jpg';
import bacon from '../../../assets/images/bacon.jpg';
import hawaiian from '../../../assets/images/hawaiian.jpg';

class Type extends Component {
    render() {
        return (
            <>
                {/* <!-- div loại pizza --> */}
                <div className="container mt-5 div-select-pizza">
                    <div className="row form-group">
                        <h1>Chọn loại pizza</h1>
                        <hr className='line'></hr>
                    </div>
                    <div className="row form-group mt-4">
                        <div className="col-sm-4 text-center">
                            {/* style="width: 100%;" */}
                            <div className="card">
                                <img className="card-img-top" src={seafood} alt="cardImage" />
                                <div className="card-body">
                                    <h5 className="card-title">OCEAN MANIA</h5>
                                    <p className="card-subtitle mb-2 text-muted">PIZZA HẢI SẢN XỐT MAYONNAISE</p>
                                    <p className="card-text">Xốt Cà Chua, Phô Mai Mozzarella, Tôm, Mực, Thanh Cua, Hành Tây.</p>
                                    <button className="btn form-control" id="btn-pizza-seafood" data-is-selected-pizza="N">Chọn</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-4 text-center">
                            <div className="card">
                                <img className="card-img-top" src={hawaiian} alt="cardImage" />
                                <div className="card-body">
                                    <h5 className="card-title">HAWAIIAN</h5>
                                    <p className="card-subtitle mb-2 text-muted">PIZZA DĂM BÔNG DỨA KIỂU HAWAII</p>
                                    <p className="card-text">Xốt Cà Chua, Phô Mai Mozzarella, Thịt Dăm Bông, Thơm.</p>
                                    <button className="btn form-control" id="btn-pizza-hawaii" data-is-selected-pizza="N">Chọn</button>
                                </div>
                            </div>
                        </div>
                        <div className="col-sm-4 text-center">
                            <div className="card">
                                <img className="card-img-top" src={bacon} alt="cardImage" />
                                <div className="card-body">
                                    <h5 className="card-title">CHEESY CHICKEN BACON</h5>
                                    <p className="card-subtitle mb-2 text-muted">PIZZA GÀ PHÔ MAI THỊT HEO XÔNG KHÓI</p>
                                    <p className="card-text">Xốt Phô Mai, Thịt Gà, Thịt Heo Muối, Phô Mai Mozzarella, Cà Chua.</p>
                                    <button className="btn form-control" id="btn-pizza-bacon" data-is-selected-pizza="N">Chọn</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}
export default Type;