import { Component } from "react";

import logoBrand from "../../assets/images/pizza-logo-58188.jpg";
import pictureSlide1 from "../../assets/images/1.jpg";

class Header extends Component {
    render() {
        return (
            <>
                <header>
                    <div className="text-header">
                        Trang Chủ
                    </div>
                    <div className="text-header">
                        Combo
                    </div>
                    <div className="text-header">
                        Loại Pizza
                    </div>
                    <div className="text-header">
                        Gửi Đơn Hàng
                    </div>
                </header>
                {/* <!-- Div slide picture --> */}
                <div className="container">
                    <div className='div-brand'>
                        <div className='div-logo-brand'>
                            <img className="img-logo-brand" src={logoBrand} alt="logo-img" />
                        </div>
                        <div className="div-content-brand">
                            <h4><i>PIZZA 365</i></h4>
                            <p><i>Truly Italian!</i></p>
                        </div>
                    </div>
                    <div className='div-carosel'>
                        <div className="row form-group">
                            <div className="col-sm-12">
                                <div id="carouselExampleControls" className="carousel slide" data-ride="carousel">
                                    <ol className="carousel-indicators">
                                        <li data-target="#carouselExampleIndicators" data-slide-to="0" className="active"></li>
                                    </ol>
                                    <div className="carousel-inner">
                                        <div className="carousel-item active">
                                            <img className="d-block w-100" src={pictureSlide1} alt="First slide" />
                                        </div>
                                    </div>
                                    <a className="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
                                        <span className="carousel-control-prev-icon" aria-hidden="true"></span>
                                        <span className="sr-only">Previous</span>
                                    </a>
                                    <a className="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
                                        <span className="carousel-control-next-icon" aria-hidden="true"></span>
                                        <span className="sr-only">Next</span>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </>
        )
    }
}

export default Header;