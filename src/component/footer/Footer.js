import { Component } from "react";
// Import thư viện fontawesome
import '@fortawesome/fontawesome-free/css/all.min.css';

class Footer extends Component {
    render() {
        return (
            <>
                <footer>
                    <div className="text-center">
                        <div className="row">
                            <div className="col-sm-12 pt-3">
                                <b>Footer</b>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12 pt-3">
                                <button className="btn btn-secondary"><i className="fas fa-arrow-up"></i> To the top</button>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12 pt-3">
                                <i className="fab fa-facebook"></i>
                                <i className="fab fa-instagram"></i>
                                <i className="far fa-bell"></i>
                                <i className="fab fa-twitter"></i>
                                <i className="fab fa-linkedin-in"></i>
                            </div>
                        </div>
                        <div className="row">
                            <div className="col-sm-12 pt-3 pb-3">
                                Powered by DEVCAMP
                            </div>
                        </div>
                    </div>
                </footer>
            </>
        )
    }
}
export default Footer;